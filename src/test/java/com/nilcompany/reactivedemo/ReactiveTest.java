package com.nilcompany.reactivedemo;

import org.junit.jupiter.api.Test;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class ReactiveTest {

	@Test
	public void printMono() {
		Mono<String> obj = Mono.just("Hello Mono").log()
				.then(Mono.error(new RuntimeException("Exception occur in Mono")));
		obj.subscribe(System.out::println, e -> System.out.println(e.getMessage()));
	}

	@Test
	public void printFlux() {
		Flux<String> obj = Flux.just("Hemant", "Priya", "Jupita", "Komal").log();
		obj.subscribe(System.out::println);
	}
}
