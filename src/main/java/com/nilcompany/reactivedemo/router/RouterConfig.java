package com.nilcompany.reactivedemo.router;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.nilcompany.reactivedemo.handler.CustomerHandler;

@Configuration
public class RouterConfig {

	private final CustomerHandler customerHandler;

	public RouterConfig(CustomerHandler customerHandler) {
		this.customerHandler = customerHandler;
	}

	@Bean
	public RouterFunction<ServerResponse> routerFunction() {
		return RouterFunctions.route()
				.GET("/router/customers", customerHandler::loadCustomers)
				.GET("/router/customers/stream", customerHandler::getCustomerStream)
				.GET("/router/customers/stream/{customerId}", customerHandler::getSingleCustomer)
				.POST("/router/customers/save", customerHandler::saveCustomer).build();
	}
}
