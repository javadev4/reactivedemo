package com.nilcompany.reactivedemo.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.nilcompany.reactivedemo.dao.CustomerDTO;
import com.nilcompany.reactivedemo.dto.Customer;

import reactor.core.publisher.Flux;

@Service
public class CustomerService {

	private final CustomerDTO customerDTO;

	public CustomerService(CustomerDTO customerDTO) {
		this.customerDTO = customerDTO;
	}

	public List<Customer> getCustomers() {
		return customerDTO.getCustomers();
	}
	
	public Flux<Customer> getCustomersStream() {
		return customerDTO.getCustomersStream();
	}
}
