package com.nilcompany.reactivedemo.handler;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.nilcompany.reactivedemo.dao.CustomerDTO;
import com.nilcompany.reactivedemo.dto.Customer;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class CustomerHandler {

	private final CustomerDTO customerDTO;

	public CustomerHandler(CustomerDTO customerDTO) {
		super();
		this.customerDTO = customerDTO;
	}

	public Mono<ServerResponse> loadCustomers(ServerRequest serverRequest) {
		Flux<Customer> customerList = customerDTO.getCustomersStream();
		return ServerResponse.ok().body(customerList, Customer.class);
	}

	public Mono<ServerResponse> getCustomerStream(ServerRequest serverRequest) {
		Flux<Customer> customerList = customerDTO.getCustomersStream();
		return ServerResponse.ok().contentType(MediaType.TEXT_EVENT_STREAM).body(customerList, Customer.class);
	}
	
	public Mono<ServerResponse> getSingleCustomer(ServerRequest serverRequest) {
		int customerId = Integer.parseInt(serverRequest.pathVariable("customerId"));
		Mono<Customer> customer = customerDTO.getCustomersStream().filter(obj -> obj.getId() == customerId).next();
		return ServerResponse.ok().contentType(MediaType.TEXT_EVENT_STREAM).body(customer, Customer.class);
	}

	public Mono<ServerResponse> saveCustomer(ServerRequest serverRequest) {
		Mono<Customer> customerMono = serverRequest.bodyToMono(Customer.class);
		Mono<String> saveResponse = customerMono.map(obj -> obj.getId() + " : " + obj.getName());
		return ServerResponse.ok().body(saveResponse, String.class);
	}
}
