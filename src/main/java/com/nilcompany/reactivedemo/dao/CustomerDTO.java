package com.nilcompany.reactivedemo.dao;

import java.time.Duration;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.stereotype.Service;

import com.nilcompany.reactivedemo.dto.Customer;

import reactor.core.publisher.Flux;

@Service
public class CustomerDTO {

	private void holtStream(int i) {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public List<Customer> getCustomers() {
		return IntStream.rangeClosed(1, 10).peek(i -> holtStream(i)).peek(i -> System.out.println("Processing : " + i))
				.mapToObj(i -> new Customer(i, "Customer " + i)).collect(Collectors.toList());
	}

	public Flux<Customer> getCustomersStream() {
		return Flux.range(1, 10).delayElements(Duration.ofSeconds(1))
				.doOnNext(i -> System.out.println("Stream Processing : " + i)).map(i -> new Customer(i, "Customer " + i));
	}

	public Flux<Customer> getCustomersStreamList() {
		return Flux.range(1, 10)
				.doOnNext(i -> System.out.println("Stream Processing : " + i)).map(i -> new Customer(i, "Customer " + i));
	}


	public List<Customer> getCustomerList() {
		return IntStream.rangeClosed(1, 10).peek(i -> System.out.println("Processing : " + i))
				.mapToObj(i -> new Customer(i, "Customer " + i)).collect(Collectors.toList());
	}
}
